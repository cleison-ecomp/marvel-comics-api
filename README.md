# Marvel Comics API
Essa aplicação faz a busca de quadrinhos de Wolverine na API da marvel. Em seguida salva os 15 primeiros em um arquivo .csv.

## Pré-requisito
É necessário o Node.js

## Para executar localmente
Clone e execute os comandos abaixo no seu terminal:

	npm install
    npm start
        acesse http://localhost:3000
