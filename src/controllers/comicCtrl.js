'use strict'

const fs = require('fs')
const axios = require('axios')
const md5 = require('md5')
const config = require('config')
const papaparse = require('papaparse')

// obtem as chaves de acordo com o contexto de execução
const marvelPublicKey = config.get('App.marvelPublicKey')
const marvelPrivateKey = config.get('App.marvelPrivateKey')

// Definir local para salvar arquivos
const path = config.get('App.path')

/**Faz a busca de quadrinhos na API da marvel e salva um arquivo .csv com os 15 primeiros resultados retornados, filtrados pela palavra chave Wolverine*/
exports.comicsToCsv = async (req, res, next) => {
  try {
    var ts = new Date().getTime()
    var hash = md5(ts+marvelPrivateKey+marvelPublicKey)
    var filter = 'Wolverine'
    var results = []
    var csvFields = ['id', 'digitalId', 'title', 'issueNumber', 'variantDescription', 'description', 'modified', 'isbn', 'upc', 'diamondCode', 'ean', 'issn', 'format', 'pageCount', 'resourceURI']
    var csvData = []
    var count = 0

    // requisição para a API da marvel
    const url = `https://gateway.marvel.com:443/v1/public/comics`
    await axios({
      method: 'get',
      url: url,
      responseType: 'application/json',
      headers: { 'Content-Type': 'application/json' },
      params: {
        format: 'comic',
        formatType: 'comic',
        title: filter,
        ts: ts,
        apikey: marvelPublicKey,
        hash: hash
      }
    }).then(response => {
      results = response.data.data.results
    }).catch(err => {
      res.status(500).send({ msg: err })
      return console.log(err)
    })

    // limita em 15 itens da lista de quadrinhos retornados
    while (count < 15) {
      let comic = results[count]
      let data = []
      csvFields.forEach(element => {
        data.push(comic[element])
      })
      csvData.push(data)

      count= count + 1;
    }

    let csv = papaparse.unparse(
        {'fields': csvFields,
        'data': csvData
        },
        {
        delimiter: ';',
      })

    // salvar arquivo
    fs.writeFile('comics.csv', csv, function (err) {
      if (err) {
        res.status(500).send({msg: err})
        return console.log(err);
      } else {
        res.status(200).send({msg: 'Arquivo .csv gerado.'})
      }
    });

  } catch (error) {
    console.log(error);
    res.status(500).send({ msg: error })
  }
}