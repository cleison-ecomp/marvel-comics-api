'use strict'

const jwt = require('jsonwebtoken')
const configs = require('../configs')

exports.generateToken = async (data) => {
  return jwt.sign(data, configs.authKey(), { expiresIn: '1d' })
}

exports.decodeToken = async (token) => {
  var data = await jwt.verify(token, configs.authKey())
  return data
}

exports.authorize = function (req, res, next) {
  var token = req.body.token || req.query.token || req.headers['token']

  if (!token) {
    res.status(401).json({
      message: 'Acesso restrito.'
    })
  } else {
    jwt.verify(token, configs.authKey(), function (error, decoded) {
      if (error) {
        res.status(401).json({
          message: 'Token Inválido.'
        })
      } else {
        next()
      }
    })
  }
}
