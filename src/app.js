'use strict'

const express = require('express')
const bodyParser = require('body-parser')

const app = express()

// Definir rotas
const comicRouter = require('./routes/comicRouter')

app.use(bodyParser.json({ limit: '50mb', extended: true }))
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }))
app.use('/', comicRouter)

module.exports = app
