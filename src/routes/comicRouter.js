'use strict'

const express = require('express')
const router = express.Router()
const authService = require('../services/authService')

const comicCtrl = require('../controllers/comicCtrl')

router.get('/', comicCtrl.comicsToCsv)
router.get('/auth', authService.authorize, comicCtrl.comicsToCsv)

module.exports = router
